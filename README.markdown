#### `Hosts file, adblock-syntax, AdGuard & uBlock Origin lists to:`
### **Block All Digital Holdings of Facebook**.
***
* ### `hosts`:
* #### **https://gitlab.com/intr0/zuckerblock/-/raw/master/ZuckerBlock.hosts**

* ### `adblock-style`:
* #### **https://gitlab.com/intr0/zuckerblock/-/raw/master/ZuckerBlock.adblock**

* ## `uBlock Origin` ruleset for Giphy:
* ## **https://gitlab.com/intr0/zuckerblock/-/raw/master/uBlockOrigin-noMoreGiphy.txt**

* ## `AdGuard specific` rulset for Giphy:
* ## **https://gitlab.com/intr0/zuckerblock/-/raw/master/AdGuard-noMoreGiphy.txt**

* ## `antiWhatsApp.hosts:`
* ## **https://gitlab.com/intr0/zuckerblock/-/raw/master/antiWhatsApp.hosts**

* ## `antiZuckerTrump.f:`
* ## **https://gitlab.com/intr0/zuckerblock/-/raw/master/antiZuckerTrump.f**

![](https://gitlab.com/intr0/zuckerblock/-/raw/master/public/img/ZuckerBlock.png)
***

##### <p xmlns:dct="https://purl.org/dc/terms/" xmlns:cc="https://creativecommons.org/ns#" class="license-text"><span rel="dct:title"></span><a href="https://creativecommons.org/publicdomain/zero/1.0"><img style="height:22px!important;margin-left: 3px;vertical-align:text-bottom;" src="https://search.creativecommons.org/static/img/cc_icon.svg" /><img  style="height:22px!important;margin-left: 3px;vertical-align:text-bottom;" src="https://search.creativecommons.org/static/img/cc-cc0_icon.svg"/></a></p>
***
##### ZuckerBlock and all lists within this repo are licensed under CC0 1.0. To view a copy of this license, visit <p xmlns:dct="http://purl.org/dc/terms/" xmlns:vcard="http://www.w3.org/2001/vcard-rdf/3.0#">
  <a rel="license"
     href="http://creativecommons.org/publicdomain/zero/1.0/">
    <img src="http://i.creativecommons.org/p/zero/1.0/88x31.png" style="border-style: none;" alt="CC0" />
  </a>
  <br />
  To the extent possible under law,
  <span rel="dct:publisher" resource="[_:publisher]">the person who associated CC0</span>
  with
  Zuckerblock
  has waived all copyright and related or neighboring rights to
  <span property="dct:title">Zuckerblock</span>.
This work is published from:
<span property="vcard:Country" datatype="dct:ISO3166"
      content="US" about="[_:publisher]">
  United States</span>.
</p>
<p>See:
<a href=https://creativecommons.org/publicdomain/zero/1.0/legalcode>CC0 legalcode</a>
